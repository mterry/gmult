/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

[GtkTemplate (ui = "/app/drey/MultiplicationPuzzle/CharBox.ui")]
public class CharBox : Adw.Bin
{
  public string letter { get; construct; default = "?"; }
  public signal void clicked();

  private Gtk.StateFlags _highlight = Gtk.StateFlags.NORMAL;
  public Gtk.StateFlags highlight {
    get {return _highlight;}
    set {
      _highlight = value;
      button.set_state_flags(value, true);
    }
  }
  
  /* Constructor */
  public CharBox (string letter)
  {
    Object(letter: letter);
  }

  construct
  {
    notify["letter"].connect(update_accessibility_label);
    update_accessibility_label();
  }

  void update_accessibility_label()
  {
    button.update_property(Gtk.AccessibleProperty.LABEL, letter);
  }

  [GtkChild]
  unowned Gtk.Button button;
  
  [GtkCallback]
  void on_button_clicked()
  {
    clicked();
  }
}

