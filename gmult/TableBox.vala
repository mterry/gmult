/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

public class TableBox : CharBox
{
  public Canvas canvas { get; construct; }
  public char character { get; construct; }
  
  public TableBox (char c, Canvas can)
  {
    Object(character: c, canvas: can, letter: "%c".printf(c));
  }
  
  construct
  {
    name = "tablebox"; // all theming is in CharBox.vala

    // Set up drag
    var drag = new Gtk.DropTarget(typeof(string), Gdk.DragAction.MOVE);
    drag.accept.connect(handle_drag_accept);
    drag.enter.connect(handle_drag_enter);
    drag.leave.connect(handle_drag_leave);
    drag.drop.connect(handle_drag_drop);
    add_controller(drag);

    // Translators: The next several characters are used to show the 'unknown'
    // digits in the puzzle.  Translate to something that makes sense for
    // your alphabet.
    N_("A");
    N_("B");
    N_("C");
    N_("D");
    N_("E");
    N_("F");
    N_("G");
    N_("H");
    N_("I");
    N_("J");
  }
  
  bool handle_drag_drop(Gtk.DropTarget target, Value val, double x, double y)
  {
    if (val.get_string().length <= 0)
      return false;
    
    int digit = val.get_string()[0] - 48;
    var rv = canvas.puzzle.guess(digit, (MultPuzzleChar)character);
    return rv == MultPuzzleGuessStatus.CORRECT;
  }

  bool handle_drag_accept(Gtk.DropTarget target, Gdk.Drop drop)
  {
    return highlight != Gtk.StateFlags.INSENSITIVE;
  }
  
  Gdk.DragAction handle_drag_enter(Gtk.DropTarget target, double x, double y)
  {
    canvas.highlight_box(this, false, true, true);
    return Gdk.DragAction.MOVE;
  }

  void handle_drag_leave(Gtk.DropTarget target)
  {
    canvas.highlight_box(this, false, false, true);
  }
}

