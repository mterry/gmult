# -*- Mode: Makefile; indent-tabs-mode: t; tab-width: 2 -*-
#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: Michael Terry

all: configure
	meson compile -C _build

%:
	@[ "$@" = "Makefile" ] || meson compile -C _build $@

configure:
	@[ -d _build/meson-info ] || meson setup _build

check: all
	meson test -C _build

dist:
	meson dist -C _build

clean:
	rm -rf _build

.PHONY: run
run:
	@flatpak run \
		--command=make \
		--devel \
		--filesystem=home \
		app.drey.MultiplicationPuzzle//master \
		run-bash

.PHONY: run-bash
run-bash:
	@env \
		PKG_CONFIG_PATH=/app/lib/pkgconfig \
		make && meson devenv -C _build gmult

.PHONY: devenv
devenv:
	@flatpak run \
		--command=make \
		--devel \
		--filesystem=home \
		--share=network \
		app.drey.MultiplicationPuzzle//master \
		devenv-bash

.PHONY: devenv-bash
devenv-bash:
	@env PKG_CONFIG_PATH=/app/lib/pkgconfig make configure
	@meson devenv -C _build env \
		-C `pwd` \
		PKG_CONFIG_PATH=/app/lib/pkgconfig \
		PS1='[📦 \W]$$ ' \
		bash --norc

.PHONY: devenv-sdk
devenv-sdk:
	flatpak remote-add --user --if-not-exists gnome-nightly https://nightly.gnome.org/gnome-nightly.flatpakrepo
	flatpak install --or-update -y gnome-nightly org.gnome.Platform//master org.gnome.Sdk//master

.PHONY: devenv-flatpak-builder
devenv-flatpak-builder:
	flatpak remote-add --user --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
	flatpak install --or-update -y flathub org.flatpak.Builder

.PHONY: devenv-setup
devenv-setup: devenv-sdk devenv-flatpak-builder flatpak
	@echo -e '\033[0;36mAll done!\033[0m Run "make devenv" to enter the build environment'

.PHONY: flatpak
flatpak:
	flatpak run org.flatpak.Builder \
		--install \
		--user \
		--force-clean \
		--state-dir=_build/.flatpak-builder \
		_build/flatpak \
		flatpak/app.drey.MultiplicationPuzzle.yaml

pot: configure
	meson compile -C _build gmult-pot
	sed -i 's/This file is distributed under the same license as the gmult package\./SPDX-License-Identifier\: GPL-3.0-or-later/' po/gmult.pot

# call like 'make copy-po TD=path-to-translation-dir'
copy-po:
	test -d $(TD)
	cp -a $(TD)/po/*.po po
	git add po/*.po

.PHONY: lint
lint:
	reuse lint

.PHONY: configure clean all check pot copy-po
