<!--
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: Michael Terry
-->

# Making a new release

- Make sure version in meson.build is accurate
- Update NEWS.md (fully technical and/or packager audience)
- Update metainfo release notes (end user audience, brief)
- Update About dialog release notes (end user audience, fuller)
- Update screenshots if the UI changed (including metainfo links)
- Update translations
- Update flathub, test it
- Tag release in git

