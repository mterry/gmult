<!--
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: Michael Terry
-->

# 14.0
- Update About dialog to modern style
- Require libadwaita 1.5, gtk 2.14, and glib 2.76

# 13.0
- Update window header to modern flat style
- Require libadwaita 1.4
- Update Croatian and French translations

# 12.0
- Refresh UI a bit to include a victory banner and toasts for feedback
- Stop showing timer in the header
- Fixed build with newer vala
- Require libadwaita 1.3 and GTK 3.10
- Change appid to app.drey.MultiplicationPuzzle
- Updated translations

# 11.0
- New icon
- New About dialog
- Remember window size between runs
- Fixed build with newer vala & mesons
- Require libadwaita 1.2
- Updated translations

# 10.0
- Updated to modern UI (gtk4 & libadwaita1)
- Updated appdata with screenshots and more metadata
- Updated translations

# 9.0
- Updated to modern UI usage, with an appmenu and a header bar
- Ship an appdata file
- Rename icon and desktop file to net.launchpad.gmult
- Require GTK 3.18 and GLib 2.40
- Updated translations

# 8.0
- Ported to Vala 0.12
- Ported to GTK+ 3
- Updated Dutch, English (UK), French, German, Malay, Russian, Serbian, and
  Turkish translations

# 7.3
- Fix odd drag icons when in compiz
- Updated Afrikaans, Brazilian Portuguese, Chinese (Simplified),
  Chinese (Traditional), Danish, Dutch, Finnish, French, Hungarian,
  Indonesian, Malay, Russian, and Turkish translations
- New Arabic, Czech, and Portuguese translation

# 7.2
- Updated for vala 0.7.1
- Updated German, Spanish, and Swedish translations
- New English (UK) translation

# 7.1
- Updated Ido and Swedish translations
- New Greek, Polish, and Spanish translations

# 7.0
- Allow guessing via the keyboard
- Allow guessing via dragging digits
- Added Hint menu option
- Added timer
- Updated vi translation

# 6.3
- Updated fr translation
- New da, io, and ru translations

# 6.2
- Updated id, it, zh_CN translations
- New hu translation

# 6.1
- Fix packaging issue that made vala necessary for compilation.

# 6.0
- Digit guesses are now made in one of two ways:
  - Click digit, then click character
  - Click character, then click digit
- Ported to vala
- Licensed under the GPL version 3
- Handles non-standard installation of icon better
- Uses svg icon
- Takes standard --help
- Requires GTK+ 2.8 and gio
- Updated vi and zh_CN translations
- New id translation

# 5.4
- New Finnish (fi) translation, courtesy of Jorma Karvonen
- New Dutch (nl) translation, courtesy of Tim Van Holder
- New Chinese (traditional) (zh_TW), courtesy of Wei-Lun Chao

# 5.3
- Put more copyright notices in source code.

# 5.2
- New Malay (ms) translation, courtesy of Sharuzzaman Ahmat Raslan
- New Swedish (sv) translation, courtesy of Daniel Nylander
- Updated Basque (eu) translation, courtesy of Mikel Olasagasti

# 5.1
- Updated French (fr) translation, courtesy of Michel Robitaille
- Updated Italian (it) translation, courtesy of Marco Colombo
- Updated Serbian (sr) translation, courtesy of Aleksandar Jelenak
- Updated Vietnemese (vi) translation, courtesy of Clytie Siddall

# 5.0
- Now requires gtkmm 2.6
- New about dialog
- Now letters grow bigger as window grows
- Fixed bug where "Congratulations!" was not displayed upon win
- New Kinyarwanda (rw) translation, courtesy of Steve Murphy
- Updated Japanese (ja) translation, courtesy of Trevor Lalish-Menagh

# 4.2
- New Italian (it) translation, courtesy of Marco Colombo

# 4.1
- New Rhaeto-Romance (rm) translation, courtesy of Florian Verdet
- Updated Brazilian Portuguese (pt_BR) translation, courtesy of Renan Mathias
  Fernandes
- Updated French (fr) translation, courtesy of Michel Robitaille
- Updated Romanian (ro) translation, courtesy of Laurentiu Buzdugan
- Updated Serbian (sr) translation, courtesy of Aleksandar Jelenak
- Updated Turkish (tr) translation, courtesy of Doruk Fisek
- Updated Vietnamese (vi) translation, courtesy of Clytie Siddall

# 4.0
- New application icon, courtesy of Eugenia Loli-Queru
- New Brazilian Portuguese (pt_BR) translation, courtesy of Leandro Cristante
  de Oliveira and Miguel Filho
- New Turkish (tr) translation, courtesy of Doruk Fisek
- New Vietnamese (vi) translation, courtesy of Clytie Siddall

# 3.0
- A little polish here and there
- Removed Help menu item, as it was empty anyway
- Updated to use gtkmm 2.4

# 2.3
- New Simplified Chinese (zh_CN) translation, courtesy of Meng Jie

# 2.2
- New Afrikaans (af) translation, courtesy of Petri Jooste
- New German (de) translation, courtesy of Roland Illig
- New Japanese (ja) translation, courtesy of Trevor Lalish-Menagh

# 2.1
- New Basque (eu) translation, courtesy of Mikel Olasagasti
- New French (fr) translation, courtesy of Michel Robitaille
- New Romanian (ro) translation, courtesy of Laurentiu Buzdugan
- New Serbian (sr) translation, courtesy of Aleksandar Jelenak

# 2.0
- Adds itself to the system menu
- New, theme-friendly styling for undiscovered digits
- Uses autoconf/automake
- Uses gettext and is now translateable
- Now is called Multiplication Puzzle

# 1.0
- Initial release of Multiplication Game
- Fully playable
